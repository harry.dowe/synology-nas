deploy-ddns-check:
	cat scripts/ddns_provider/ddns_check.sh | ssh nas "cat - > /var/services/homes/dowe/scripts/ddns_provider/ddns_check.sh"
	cat scripts/ddns_provider/cloudflaredns_domainname.sh | ssh nas "cat - > /var/services/homes/dowe/scripts/ddns_provider/cloudflaredns_domainname.sh"
	cat scripts/ddns_provider/cloudflare.conf | ssh nas "cat - > /var/services/homes/dowe/scripts/ddns_provider/cloudflare.conf"
	ssh nas chmod +x /var/services/homes/dowe/scripts/ddns_provider/ddns_check.sh /var/services/homes/dowe/scripts/ddns_provider/cloudflaredns_domainname.sh

deploy-nas-media-redirect:
	cat web/nas-media-redirect/src/index.php | ssh nas "cat - > /volume1/web/nas-media-redirect/index.php"

deploy-cert-renew-script:
	cat scripts/certs/renew.sh | ssh nas "cat - > /var/services/homes/dowe/scripts/certs/renew.sh"
	cat scripts/certs/reload-certs.sh | ssh nas "cat - > /var/services/homes/dowe/scripts/certs/reload-certs.sh"
	ssh nas "chmod +x /var/services/homes/dowe/scripts/certs/renew.sh /var/services/homes/dowe/scripts/certs/reload-certs.sh"
