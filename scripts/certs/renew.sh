#!/bin/bash

subDomains=(
	"nas"
	"drive"
	"media"
	"drive-client"
	"photos"
	"api"
)

for subDomain in "${subDomains[@]}"; do
	/usr/local/share/acme.sh/acme.sh \
		--renew \
		--domain "$subDomain.harrydowe.uk" \
		--home /usr/local/share/acme.sh \
		&& /usr/local/share/acme.sh/acme.sh \
			--domain "$subDomain.harrydowe.uk" \
			--deploy \
			--deploy-hook synology_dsm \
			--reloadcmd /var/services/homes/dowe/scripts/certs/reload-certs.sh \
			--home /usr/local/share/acme.sh
done
