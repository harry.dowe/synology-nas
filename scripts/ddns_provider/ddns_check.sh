#!/bin/bash

set -e;

if ! grep -Lq "ddns_check_start" /etc.defaults/ddns_provider.conf; then
    echo "Applying config"
	cat /var/services/homes/dowe/scripts/ddns_provider/cloudflare.conf >> /etc.defaults/ddns_provider.conf
fi

if [[ ! -f /sbin/cloudflaredns_domainname.sh ]]; then
    echo "Copying cloudflaredns_domainname.sh"
    cp /var/services/homes/dowe/scripts/ddns_provider/cloudflaredns_domainname.sh /sbin/cloudflaredns_domainname.sh
fi
