# `ddns_provider`

## `ddns_check.sh`
This script repairs the DDNS configuration by inspecting the `/etc.defaults/ddns_provider.conf` file
and appending any missing configuration. It also ensures `cloudflaredns_domainname.sh` is in the `/sbin/` directory.

## `cloudflaredns_domainname.sh`

This script is taken from https://github.com/joshuaavalon/SynologyCloudflareDDNS

It performs the API calls to Cloudflare to update the DNS records with the new IP address.
