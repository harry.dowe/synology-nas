# `nas-media-redirect`

This app lives in `/volume1/web/nas-media-redirect`.

It redirects the hostname to the corresponding service via a reverse proxy.
The hostnames are configured on the client's machine by modifying their host file.
