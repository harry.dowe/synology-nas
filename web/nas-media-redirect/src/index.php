<?php

declare(strict_types=1);

function main(): void
{
    $location = getLocation($_SERVER['HTTP_HOST'] ?? null, 'http://192.168.1.25');

    if ($location !== null) {
        header("Location: {$location}");
    }
}

function getLocation(?string $httpHost, string $destination): ?string
{
    return match ($httpHost) {
        'nas.local.harrydowe.uk' => "http://{$destination}:5000",
        'drive.local.harrydowe.uk' => "https://{$destination}:10003",
        'media.local.harrydowe.uk' => "http://{$destination}:32400",
        'api.local.harrydowe.uk' => "http://{$destination}:8000",
        default => null,
    };
}

main();
